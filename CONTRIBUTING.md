# Contributing to SAGACE SDK

Welcome to the **SAGACE SDK** repository! 🎉  

This project follows **the best software development practices**, ensuring **code quality, maintainability, and SOLID principles**.  
By contributing, you agree to **adhere to these standards** and **maintain consistency** in the repository.

---

## ⚖ **Project Philosophy: SOLID Principles**
This repository strictly follows the **SOLID** principles to ensure a scalable and maintainable codebase:

- **S**ingle Responsibility Principle (**SRP**) - Each class should have only one reason to change.
- **O**pen/Closed Principle (**OCP**) - Extend functionality without modifying existing code.
- **L**iskov Substitution Principle (**LSP**) - Subtypes should be interchangeable with their base types.
- **I**nterface Segregation Principle (**ISP**) - Avoid forcing classes to implement unused methods.
- **D**ependency Inversion Principle (**DIP**) - Depend on abstractions, not concrete implementations.

Every contribution **must respect these principles** to maintain code integrity.

---

## ✅ **Best Practices for Contributing**
### 📌 **1. Fork and Clone**
1. **Fork the repository** on GitLab.
2. Clone your forked repository:
   ```sh
   git clone git@gitlab.com:your-username/sagace-sdk.git
   cd sagace-sdk
   ```

### 📌 **2. Create a New Branch**
- **Use meaningful branch names** following this format:  
  ```
  feature/{short-description}  →  For new features
  fix/{short-description}      →  For bug fixes
  chore/{short-description}    →  For maintenance tasks
  docs/{short-description}     →  For documentation updates
  ```
- Example:
  ```sh
  git checkout -b feature/authentication-improvements
  ```

### 📌 **3. Follow Git Commit Best Practices**
- **Commit messages must be structured** and follow the format:
  ```
  type: Short and clear description (max 72 characters)
  
  Detailed explanation of the changes.
  ```
- **Valid commit types:**
  - `feat:` → A new feature.
  - `fix:` → A bug fix.
  - `docs:` → Documentation updates.
  - `refactor:` → Code refactoring without functionality change.
  - `test:` → Adding or updating tests.
  - `chore:` → Maintenance, dependencies, or CI updates.

- **Example of a good commit message:**
  ```
  feat: Implement JWT authentication mechanism

  - Added `AuthenticateUser` class following SRP.
  - Implemented `AuthenticationRepository` interface.
  - Updated tests to validate authentication flow.
  ```

### 📌 **4. Keep the Code Clean**
- **Follow PEP8** coding style.
- Ensure **all functions and classes have Sphinx-compatible docstrings**.
- **Avoid unnecessary dependencies** and keep `requirements.txt` minimal.
- Run **type checking** with `mypy`:
  ```sh
  mypy sagace/
  ```
- Format your code using **Black and isort**:
  ```sh
  black sagace/
  isort sagace/
  ```

### 📌 **5. Run Tests Before Pushing**
- **Ensure all tests pass before submitting a merge request.**
- Run **unit tests with pytest**:
  ```sh
  pytest tests/
  ```

---

## 🚀 **Automatic PyPI Deployment on `main`**
This project has **CI/CD integration with PyPI**:  
**Whenever a commit is pushed to `main`, the package is automatically published to PyPI.**

### 📌 **Before Merging to `main`, Follow These Steps:**
1. **Update version numbers** in:
   - `setup.py`
   - `setup.cfg`
   - `pyproject.toml`

2. **Tag the commit with the new version** (following [Semantic Versioning](https://semver.org/)):
   ```sh
   git tag vX.Y.Z
   git push origin vX.Y.Z
   ```

3. **Ensure all tests pass** and **the new version is stable** before merging.

---

## 📄 **Submitting a Merge Request (MR)**
1. Push your branch:
   ```sh
   git push origin feature/authentication-improvements
   ```
2. Go to **GitLab → Merge Requests → New Merge Request**.
3. Select **your branch** and target **`develop`** (never push directly to `main`).
4. Fill in the MR description **clearly explaining the changes**.
5. **Tag a reviewer** before submission.

---

## 🎯 **Final Checklist Before Submitting**
✅ Code follows **SOLID principles**  
✅ All **tests pass** (`pytest tests/`)  
✅ Code is formatted (`black`, `isort`)  
✅ **Version number updated** (if merging to `main`)  
✅ **Git tag added** (if releasing to PyPI)  

By following these guidelines, we ensure a **high-quality and maintainable project**.  
Thank you for your contributions! 🚀

