.. sagace documentation master file, created by
   sphinx-quickstart on Thu Feb 27 21:40:35 2025.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================================
SAGACE SDK Documentation
=================================

Welcome to the official documentation for **SAGACE SDK**.

.. toctree::
   :maxdepth: 2
   :caption: Conteúdo:

   modules