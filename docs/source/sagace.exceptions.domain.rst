sagace.exceptions.domain package
================================

Submodules
----------

sagace.exceptions.domain.api\_request\_error module
---------------------------------------------------

.. automodule:: sagace.exceptions.domain.api_request_error
   :members:
   :show-inheritance:
   :undoc-members:

sagace.exceptions.domain.domain\_error module
---------------------------------------------

.. automodule:: sagace.exceptions.domain.domain_error
   :members:
   :show-inheritance:
   :undoc-members:

sagace.exceptions.domain.permission\_denied\_error module
---------------------------------------------------------

.. automodule:: sagace.exceptions.domain.permission_denied_error
   :members:
   :show-inheritance:
   :undoc-members:

sagace.exceptions.domain.token\_expired\_error module
-----------------------------------------------------

.. automodule:: sagace.exceptions.domain.token_expired_error
   :members:
   :show-inheritance:
   :undoc-members:

Module contents
---------------

.. automodule:: sagace.exceptions.domain
   :members:
   :show-inheritance:
   :undoc-members:
