sagace.exceptions.infrastructure package
========================================

Submodules
----------

sagace.exceptions.infrastructure.authentication\_error module
-------------------------------------------------------------

.. automodule:: sagace.exceptions.infrastructure.authentication_error
   :members:
   :show-inheritance:
   :undoc-members:

sagace.exceptions.infrastructure.infrastructure\_error module
-------------------------------------------------------------

.. automodule:: sagace.exceptions.infrastructure.infrastructure_error
   :members:
   :show-inheritance:
   :undoc-members:

Module contents
---------------

.. automodule:: sagace.exceptions.infrastructure
   :members:
   :show-inheritance:
   :undoc-members:
