sagace.exceptions.interfaces package
====================================

Submodules
----------

sagace.exceptions.interfaces.interface\_error module
----------------------------------------------------

.. automodule:: sagace.exceptions.interfaces.interface_error
   :members:
   :show-inheritance:
   :undoc-members:

Module contents
---------------

.. automodule:: sagace.exceptions.interfaces
   :members:
   :show-inheritance:
   :undoc-members:
