sagace.exceptions package
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   sagace.exceptions.application
   sagace.exceptions.domain
   sagace.exceptions.infrastructure
   sagace.exceptions.interfaces

Module contents
---------------

.. automodule:: sagace.exceptions
   :members:
   :show-inheritance:
   :undoc-members:
