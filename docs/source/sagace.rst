sagace package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   sagace.auth
   sagace.core
   sagace.exceptions
   sagace.samples

Module contents
---------------

.. automodule:: sagace
   :members:
   :show-inheritance:
   :undoc-members:
